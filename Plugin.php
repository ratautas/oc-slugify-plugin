<?php

namespace Ratauto\Slugify;

use Cocur\Slugify\Slugify;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name' => 'Slugify Plugin',
            'description' => 'Extends TWIG by with a filter to convert string to slug i.e.: {{STING|slugify}} ',
            'author' => 'Algirdas Tamasauskas',
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'slugify' => [$this, 'slugify'],
            ],
        ];
    }

    public function slugify($text)
    {
        $slugify = new Slugify();

        return $slugify->slugify($text);
    }
}
